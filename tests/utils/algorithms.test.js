
const { getSupportedCiphers, getSupportedHashes } = require('../../src/routes/utils/algorithms');
test('Supported Ciphers', async () => {
    let ciphers = await getSupportedCiphers();
    console.log(JSON.stringify(ciphers));
    expect(ciphers.length).toBe(119);
});

test('Supported Hashes', async () => {
    let hashes = await getSupportedHashes();
    expect(hashes.length).toBe(46);
});