# README #

### What is this repository for? ###

* This project is to maintain the testing suite that will evaluate the performance of various encryption algorithms

### How do I get set up? ###

* Must have node (minimum v8) installed. Refer to installation steps here https://nodejs.org/en/download/.
* If you do not have yarn installed then run: `npm install -g yarn`
* run: `yarn install`
* run : `docker run -itd --name=mongo -p 27017:27017 mongo:latest` to spin up mongodb container. This will be used to write performance document to.
* run: `yarn start` to run the service.
* If you want to test the service manually you can `curl localhost:8080/encryptions?type=<symmetrics or asymmetric>&algo=<algorithm>`
* For a list of supported algorithms see the `symmetric-test.yml` file in the `/load-tests` directory.

### Contribution guidelines ###
* Writing Crypto API
    * The necessary crypto functions provided by the nodejs crypto module will be exposed via an API. All that is necessary is to expose a GET operation on an endpoint where we can specify the algorithm and type(asymmetric or symmetric encryption)
* Writing tests
    * Load test are all configured using yml files. Artillery is the framework that this project uses to conduct all load tests. You can find documentation at https://artillery.io/.
    * Unit tests are written in the `/tests/*` src directory. These tests are executed using jest. You can run the tests using the command `yarn test`. Documentation on jest can be found here https://jestjs.io/docs/en/getting-started.html.
* Running tests
    * Load tests are executed using `yarn load-test`. The service must be running first.
    * Unit tests are executed using `yarn test`
### Who do I talk to? ###

* Anyone on the team