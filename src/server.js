const express = require('express'),
    app = express(),
    fs = require('fs'),
    path = require('path'),
    body = require('body-parser'),
    lessMiddleware = require('less-middleware'),
    log = require('morgan'),
    errorHandler = require('errorhandler'),
    logDir = path.join(__dirname, '..', 'log'),
    route = require('./routes/index'),
    HOST = process.env.HOST || '0.0.0.0',
    PORT = process.env.PORT || 3000;

// -- [ Setup log dir
fs.existsSync(logDir) || fs.mkdirSync(logDir);
let logStream = fs.createWriteStream(path.join(__dirname, '..', 'log', 'access.log'), {
    flags: 'a'
});

// -- [ Setup app usage "MIDDLEWARE"
app.use(log('combined', { stream: logStream }));
app.use(body.json());
app.use(body.urlencoded({ extended: true }));
app.use(lessMiddleware(path.join(__dirname, 'frontend'), {
    force: true // -- [ Only for DEV, remove in production
}));
app.use('/', express.static(path.join(__dirname, 'frontend')));
app.use('/node', express.static(path.join(__dirname, '..', 'node_modules')));
app.use(errorHandler({
    dumpExeptions: true,
    showStack: true
}));

// -- [ Pug Template Usage
app.set('views', path.join(__dirname, 'frontend', 'pug'));
app.set('view engine', 'pug');

app.get('/', (req, res) => { res.render('index'); });
app.use('/encryptions', route); // -- [ For API when enabled

const http = require('http').createServer(app);
http.listen(PORT, () => {
    console.log('Server: %s:%s', HOST, PORT);
});
