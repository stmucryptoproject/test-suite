angular.module('cipherSuite')
.factory('API', function ($http) {
    const url = 'encryptions/';
    return {
        asym: () => {
            return $http({
                method: 'GET',
                url: url + 'asymmetric',
                params: {}
            }).then(function (res) {
                return res.data;
            });
        },
        sym: () => {
            return $http({
                method: 'GET',
                url: url + 'symmetric',
            }).then(function (res) {
                return res.data;
            });
        },
        custom: (params) => {
            // console.log('params: ', params);
            if (params.type === 'asymmetric') {
                params.keySize = params.algorithm.split('-')[1];
                params.algorithm = params.algorithm.split('-')[0];
            }

            return $http({
                method: 'GET',
                url: url + 'custom',
                params: {
                    data: params.plainText,
                    algo: params.algorithm,
                    keySize: params.keySize ? params.keySize : null,
                    type: params.type
                }
            }).then((res) => {
                return res.data;
            });
        },
        settings: (params) => {
            return $http({
                method: 'GET',
                url: url + 'settings',
                params: {type: params.type}
            }).then((res) => {
                return res.data;
            })
        }
    }
});