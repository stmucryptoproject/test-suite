angular.module('cipherSuite', [
    'ngAria',
    'ngAnimate',
    'ngMdIcons',
    'ngRoute',
    'ngMaterial',
    'ng-fusioncharts'
]).config([
    '$mdThemingProvider',
    '$routeProvider',
    function ($mdThemingProvider, $routeProvider) {
        // -- [ Theming
        $mdThemingProvider
            .theme('default')
            .primaryPalette('blue')
            .accentPalette('blue-grey')
            .warnPalette('red')
            .backgroundPalette('grey')
            .dark();

        $mdThemingProvider
            .theme('dark-blue')
            .backgroundPalette('blue')
            .dark();

        // -- [ Routing
        $routeProvider
            .when('/', {
                templateUrl: 'templates/custom.html',
                controller: 'customCtrl'})
            .when('/asymmetric', {
                templateUrl: 'templates/asymmetric.html',
                controller: 'asymmCtrl'})
            .when('/symmetric', {
                templateUrl: 'templates/symmetric.html',
                controller: 'symmCtrl'})
            .otherwise({ redirectTo: '/' });
    }
]);
