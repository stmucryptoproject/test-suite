angular.module('cipherSuite')
.controller('customCtrl', function ($scope, API) {
    $scope.selectedParams = {
        type: null,
        algorithm: null,
        keySize: null,
        sizes: [],
        algorithmList: [],
        inputType: null,
        uploadFile: null,
        plainText: null,
        password: null,
    };

    $scope.results = {
        algorithm: null,
        output: null,
        input: null,
        delay: null,
        iv: null,
        key: null,
        keyPair: {
            private: null,
            public: null
        }
    };
    
    $scope.delayData = {
        decryptSource: {
            chart: {
                caption: 'Decryption Delay',
                lowerlimit: 0,
                upperlimit: 1000,
                showvalue: 1,
                numbersuffix: ' μS',
                theme: 'candy',
                showtooltip: 1
            },
            colorrange: {
                color: [
                    { minvalue: 0, maxvalue: 500, code: '#62B58F' },
                    { minvalue: 500, maxvalue: 750, code: '#FFC533' },
                    { minvalue: 750, maxvalue: 1000, code: '#F2726F' }
                ]
            },
            dials: {
                dial: [{ value: 0 }]
            }
        },
        encryptSource: {
            chart: {
                caption: 'Encryption Delay',
                lowerlimit: 0,
                upperlimit: 1000,
                showvalue: 1,
                numbersuffix: ' μS',
                theme: 'candy',
                showtooltip: 1
            },
            colorrange: {
                color: [
                    { minvalue: 0, maxvalue: 500, code: '#62B58F' },
                    { minvalue: 500, maxvalue: 750, code: '#FFC533' },
                    { minvalue: 750, maxvalue: 1000, code: '#F2726F' }
                ]
            },
            dials: {
                dial: [{ value: 0 }]
            }
        },
        keyPairSource: {
            chart: {
                caption: 'Key Pair Delay',
                lowerlimit: 0,
                upperlimit: 1000,
                showvalue: 1,
                numbersuffix: ' μS',
                theme: 'candy',
                showtooltip: 1
            },
            colorrange: {
                color: [
                    { minvalue: 0, maxvalue: 500, code: '#62B58F' },
                    { minvalue: 500, maxvalue: 750, code: '#FFC533' },
                    { minvalue: 750, maxvalue: 1000, code: '#F2726F' }
                ]
            },
            dials: {
                dial: [{ value: 0 }]
            }
        }
    };

    API.settings({type: 'custom'}).then((res) => {
        $scope.encryptions = res;
        // console.log('encrypt: ', $scope.encryptions);
    });

    $scope.getAlgorithms = () => {
        $scope.selectedParams.algorithmList = [];
        $scope.selectedParams.sizes = [];
        $scope.selectedParams.algorithm = null;
        if ($scope.selectedParams.type === 'symmetric') {
            $scope.selectedParams.algorithmList = $scope.encryptions.sym;
        } else if ($scope.selectedParams.type === 'asymmetric') {
            $scope.selectedParams.algorithmList = $scope.encryptions.asym;
        } else {
            $scope.selectedParams.algorithmList = $scope.encryptions.hash;
        }
    };

    $scope.submit = () => {
        API.custom($scope.selectedParams).then((res) => {
            resetResults();
            // console.log('RES: ', res);
            $scope.results.algorithm = res.algo;
            $scope.results.output = res.cipher;
            $scope.results.input = res.plainText;
            $scope.delayData.decryptSource.dials.dial[0].value = res.decryptElapsedSeconds ? res.decryptElapsedSeconds * 1000000 : 0;
            $scope.delayData.encryptSource.dials.dial[0].value = res.elapsedSeconds * 1000000;
            $scope.results.keySize = res.keySize;
            $scope.results.key = res.key;
            $scope.results.iv = res.iv;

            if (res.algo === 'rsa' || res.algo === 'ec') {
                $scope.delayData.encryptSource.dials.dial[0].value = res.encryptElapsedSeconds * 1000000;
                $scope.delayData.keyPairSource.dials.dial[0].value = res.keyPair.elapsedSeconds * 1000000;
                if (res.algo === 'rsa') {
                    $scope.results.keyPair.public = res.keyPair.publicKey;
                    $scope.results.keyPair.private = res.keyPair.privateKey;
                }
            }
        });

        $scope.reset();
    };

    $scope.reset = () => {
        $scope.selectedParams = {
            type: null,
            algorithm: null,
            keySize: null,
            sizes: [],
            algorithmList: [],
            inputType: null,
            uploadFile: null,
            plainText: null,
            password: null,
        };
    };

    function resetResults() {
        $scope.results = {
            algorithm: null,
            output: null,
            input: null,
            delay: null,
            iv: null,
            key: null,
            keyPair: {
                private: null,
                public: null
            }
        };
        $scope.delayData.keyPairSource.dials.dial[0].value = 0;
        $scope.delayData.encryptSource.dials.dial[0].value = 0;
        $scope.delayData.decryptSource.dials.dial[0].value = 0;
    }
});
