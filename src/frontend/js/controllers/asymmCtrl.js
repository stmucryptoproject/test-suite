angular.module('cipherSuite')
.controller('asymmCtrl', function ($scope, API) {
    $scope.asymmetricDecryptData = {
        chart: {
            caption: 'Decryption Time',
            yaxisname: 'Time in microseconds',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    };
    $scope.decAvgData = {
        chart: {
            caption: 'Decryption Avg Time',
            lowerlimit: 0,
            upperlimit: 150,
            showvalue: 1,
            numbersuffix: ' μS',
            theme: 'candy',
            showtooltip: 1
        },
        colorrange: {
            color: [
                { minvalue: 0, maxvalue: 2000, code: '#62b58f' },
                { minvalue: 2000, maxvalue: 3000, code: '#ffc533' },
                { minvalue: 3000, maxvalue: 4000, code: '#f2726f' }
            ]
        },
        dials: {
            dial: [{ value: 0 }]
        }
    };

    $scope.encAvgData = {
        chart: {
            caption: 'Encryption Avg Time',
            lowerlimit: 0,
            upperlimit: 150,
            showvalue: 1,
            numbersuffix: ' μS',
            theme: 'candy',
            showtooltip: 1
        },
        colorrange: {
            color: [
                { minvalue: 0, maxvalue: 2000, code: '#62b58f' },
                { minvalue: 2000, maxvalue: 3000, code: '#ffc533' },
                { minvalue: 3000, maxvalue: 4000, code: '#f2726f' }
            ]
        },
        dials: {
            dial: [{ value: 0 }]
        }
    };

    $scope.asymmetricEncryptData = {
        chart: {
            caption: 'Encryption Time',
            yaxisname: 'Time in microseconds',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    };

    $scope.asymmetricKeyData = {
        chart: {
            caption: 'Key Generation Time',
            yaxisname: 'Time in microseconds',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    }

    $scope.asymmetricKeyRsa1024VsEC160 = {
        chart: {
            caption: 'Key Generation Time (Rsa-1024 Vs EC-160)',
            yaxisname: 'Time in microseconds',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    }


    $scope.asymmetricKeyRsa2048VsEC224 = {
        chart: {
            caption: 'Key Generation Time (Rsa-2048 Vs EC-224)',
            yaxisname: 'Time in microseconds',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    }


    $scope.asymmetricKeyRsa3072VsEC256 = {
        chart: {
            caption: 'Key Generation Time (Rsa-3072 Vs EC-256)',
            yaxisname: 'Time in microseconds',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    }


    $scope.asymmetricKeyRsa7680VsEC384 = {
        chart: {
            caption: 'Key Generation Time (Rsa-7680 Vs EC-384)',
            yaxisname: 'Time in microseconds',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    }


    $scope.asymmetricKeyRsa15360VsEC521 = {
        chart: {
            caption: 'Key Generation Time (Rsa-15360 Vs EC-521)',
            yaxisname: 'Time in microseconds',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    }

    $scope.fastSlowData = {
        chart: {
            caption: 'Fastest & Slowest Algorithms',
            xAxisName: 'Algorithm',
            yAxisName: 'Timing in μS',
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: []
    }

    function getAverage(elemArr) {
        let tot = 0;
        elemArr.forEach((elem) => { tot += elem; });
        return tot / elemArr.length;
    }

    API.asym().then((res) => {
        let delayArr = Array(),
            totalArr = Array(),
            fastSlow = Array();

        let abc = res.map((elem) => {
            let tmp = {};
        
            elem.decryptElapsedSeconds *= 1000000;
            elem.encryptElapsedSeconds *= 1000000;
            elem.keyPair.elapsedSeconds *= 1000000;

            $scope.asymmetricDecryptData.data.push({ label: `${elem.algo} - ${elem.keySize}`, value: elem.decryptElapsedSeconds });
            $scope.asymmetricEncryptData.data.push({ label: `${elem.algo} - ${elem.keySize}`, value: elem.encryptElapsedSeconds });

            if((elem.algo === 'ec' && elem.keySize ===160) || (elem.algo === 'rsa' && elem.keySize === 1024)) {
                $scope.asymmetricKeyRsa1024VsEC160.data.push({ label: `${elem.algo} - ${elem.keySize}`, value: elem.keyPair.elapsedSeconds });
            }


            if((elem.algo === 'ec' && elem.keySize === 224) || (elem.algo === 'rsa' && elem.keySize === 2048)) {
                $scope.asymmetricKeyRsa2048VsEC224.data.push({ label: `${elem.algo} - ${elem.keySize}`, value: elem.keyPair.elapsedSeconds });
            }


            if((elem.algo === 'ec' && elem.keySize === 256) || (elem.algo === 'rsa' && elem.keySize === 3072)) {
                $scope.asymmetricKeyRsa3072VsEC256.data.push({ label: `${elem.algo} - ${elem.keySize}`, value: elem.keyPair.elapsedSeconds });
            }


            if((elem.algo === 'ec' && elem.keySize === 384) || (elem.algo === 'rsa' && elem.keySize === 7680)) {
                $scope.asymmetricKeyRsa7680VsEC384.data.push({ label: `${elem.algo} - ${elem.keySize}`, value: elem.keyPair.elapsedSeconds });
            }


            if((elem.algo === 'ec' && elem.keySize === 521) || (elem.algo === 'rsa' && elem.keySize === 15360)) {
                $scope.asymmetricKeyRsa15360VsEC521.data.push({ label: `${elem.algo} - ${elem.keySize}`, value: elem.keyPair.elapsedSeconds });
            }



            delayArr.push(elem.decryptElapsedSeconds);
            totalArr.push(elem.encryptElapsedSeconds);

            tmp.algo = elem.algo;
            tmp.decryptDelay = elem.decryptElapsedSeconds;
            tmp.keySize = elem.keySize;

            return tmp;
        });

        let def = res.map((elem) => {
            let tmp = {};

            tmp.algo = elem.algo;
            tmp.encryptDelay = elem.encryptElapsedSeconds;
            tmp.keySize = elem.keySize;

            return tmp;
        });

        decryptTiming = abc.sort((a,b) => { return b.decryptDelay - a.decryptDelay });
        encryptTiming = def.sort((a,b) => { return b.encryptDelay - a.encryptDelay });

        for (let i = 0; i < decryptTiming.length; i++) {
            if (i < 3) {
                fastSlow.push({ label: 'Decryption: ' + `${decryptTiming[i].algo} - ${decryptTiming[i].keySize}`, value: decryptTiming[i].decryptDelay });
            } else if (i > decryptTiming.length - 4) {
                fastSlow.push({ label: 'Decryption: ' + `${decryptTiming[i].algo} - ${decryptTiming[i].keySize}`, value: decryptTiming[i].decryptDelay });
            }
        }

        for (let i = 0; i < encryptTiming.length; i++) {
            if (i < 3) {
                fastSlow.push({ label: 'Encryption: ' + `${encryptTiming[i].algo} - ${encryptTiming[i].keySize}`, value: encryptTiming[i].encryptDelay });
            } else if (i > decryptTiming.length - 4) {
                fastSlow.push({ label: 'Encryption: ' + `${encryptTiming[i].algo} - ${encryptTiming[i].keySize}`, value: encryptTiming[i].encryptDelay });
            }
        }

        let tmpDelay = getAverage(delayArr),
            tmpTotal = getAverage(totalArr);

        $scope.fastSlowData.data = fastSlow;
        $scope.decAvgData.dials.dial[0].value = tmpDelay;
        $scope.encAvgData.dials.dial[0].value = tmpTotal;
        $scope.asymmetricDecryptData.trendlines[0].line[0].displayvalue = 'Average ' + tmpDelay.toFixed(2) + ' μS';
        $scope.asymmetricDecryptData.trendlines[0].line[0].startvalue = tmpDelay;
        $scope.asymmetricEncryptData.trendlines[0].line[0].displayvalue = 'Average ' + tmpTotal.toFixed(2) + ' μS';
        $scope.asymmetricEncryptData.trendlines[0].line[0].startvalue = tmpTotal;
    });
});