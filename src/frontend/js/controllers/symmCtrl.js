angular.module('cipherSuite')
.controller('symmCtrl', function ($scope, API) {
    $scope.symmetricDecryptData = {
        chart: {
            caption: 'Decryption Time',
            yaxisname: 'Time in microseconds (μS)',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    };
    $scope.decAvgData = {
        chart: {
            caption: 'Decryption Avg Time',
            lowerlimit: 0,
            upperlimit: 150,
            showvalue: 1,
            numbersuffix: ' μS',
            theme: 'candy',
            showtooltip: 1
        },
        colorrange: {
            color: [
                { minvalue: 0, maxvalue: 75, code: '#62b58f' },
                { minvalue: 75, maxvalue: 115, code: '#ffc533' },
                { minvalue: 115, maxvalue: 150, code: '#f2726f' }
            ]
        },
        dials: {
            dial: [{ value: 0 }]
        }
    };

    $scope.encAvgData = {
        chart: {
            caption: 'Encryption Avg Time',
            lowerlimit: 0,
            upperlimit: 150,
            showvalue: 1,
            numbersuffix: ' μS',
            theme: 'candy',
            showtooltip: 1
        },
        colorrange: {
            color: [
                { minvalue: 0, maxvalue: 75, code: '#62b58f' },
                { minvalue: 75, maxvalue: 115, code: '#ffc533' },
                { minvalue: 115, maxvalue: 150, code: '#f2726f' }
            ]
        },
        dials: {
            dial: [{ value: 0 }]
        }
    };

    $scope.symmetricEncryptData = {
        chart: {
            caption: 'Encryption Time',
            yaxisname: 'Time in microseconds (μS)',
            aligncaptionwithcanvas: 0,
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: [],
        trendlines: [{
            line: [{
                startvalue: 0,
                color: '#29c3be',
                displayvalue: null
            }]
        }]
    };

    $scope.fastSlowData = {
        chart: {
            caption: 'Fastest & Slowest Algorithms',
            xAxisName: 'Algorithm',
            yAxisName: 'Timing in μS',
            plottooltext: '<b>$dataValue</b> μS',
            theme: 'candy'
        },
        data: []
    }

    function getAverage(elemArr) {
        let tot = 0;
        elemArr.forEach((elem) => { tot += elem; });
        return tot / elemArr.length;
    }

    API.sym().then((res) => {
        let delayArr = Array(),
            totalArr = Array(),
            fastSlow = Array();

        let abc = res.map((elem) => {
            let tmp = {};
        
            elem.decryptElapsedSeconds *= 1000000;
            elem.elapsedSeconds *= 1000000;

            $scope.symmetricDecryptData.data.push({ label: elem.algo, value: elem.decryptElapsedSeconds });
            $scope.symmetricEncryptData.data.push({ label: elem.algo, value: elem.elapsedSeconds });

            delayArr.push(elem.decryptElapsedSeconds);
            totalArr.push(elem.elapsedSeconds);

            tmp.algo = elem.algo;
            tmp.decryptDelay = elem.decryptElapsedSeconds;

            return tmp;
        });

        let def = res.map((elem) => {
            let tmp = {};

            tmp.algo = elem.algo;
            tmp.encryptDelay = elem.elapsedSeconds;

            return tmp;
        });

        decryptTiming = abc.sort((a,b) => { return b.decryptDelay - a.decryptDelay });
        encryptTiming = def.sort((a,b) => { return b.encryptDelay - a.encryptDelay });

        for (let i = 0; i < decryptTiming.length; i++) {
            if (i < 3) {
                fastSlow.push({ label: 'Decryption: ' + decryptTiming[i].algo, value: decryptTiming[i].decryptDelay });
            } else if (i > decryptTiming.length - 4) {
                fastSlow.push({ label: 'Decryption: ' + decryptTiming[i].algo, value: decryptTiming[i].decryptDelay });
            }
        }

        for (let i = 0; i < encryptTiming.length; i++) {
            if (i < 3) {
                fastSlow.push({ label: 'Encryption: ' + encryptTiming[i].algo, value: encryptTiming[i].encryptDelay });
            } else if (i > decryptTiming.length - 4) {
                fastSlow.push({ label: 'Encryption: ' + encryptTiming[i].algo, value: encryptTiming[i].encryptDelay });
            }
        }
        // fastSlow.push({ label: 'Decryption: ' + decryptTiming[decryptTiming.length - 1].algo, value: decryptTiming[decryptTiming.length - 1].decryptDelay });
        // fastSlow.push({ label: 'Decryption: ' + decryptTiming[0].algo, value: decryptTiming[0].decryptDelay });
        // fastSlow.push({ label: 'Encryption: ' + encryptTiming[encryptTiming.length - 1].algo, value: encryptTiming[encryptTiming.length - 1].encryptDelay });
        // fastSlow.push({ label: 'Encryption: ' + encryptTiming[0].algo, value: encryptTiming[0].encryptDelay });

        $scope.fastSlowData.data = fastSlow;

        let tmpDelay = getAverage(delayArr),
            tmpTotal = getAverage(totalArr);

        $scope.decAvgData.dials.dial[0].value = tmpDelay;
        $scope.encAvgData.dials.dial[0].value = tmpTotal;
        $scope.symmetricDecryptData.trendlines[0].line[0].displayvalue = 'Average ' + tmpDelay.toFixed(2) + ' μS';
        $scope.symmetricDecryptData.trendlines[0].line[0].startvalue = tmpDelay;
        $scope.symmetricEncryptData.trendlines[0].line[0].displayvalue = 'Average ' + tmpTotal.toFixed(2) + ' μS';
        $scope.symmetricEncryptData.trendlines[0].line[0].startvalue = tmpTotal;
    });
});