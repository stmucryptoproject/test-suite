exports.getElapsedTime = (startTime, precision) => {
    const elapsed = process.hrtime(startTime);
    const elapsedSeconds = (elapsed[0] + (elapsed[1] / 1e9)).toFixed(precision || 3);
    return elapsedSeconds;
}