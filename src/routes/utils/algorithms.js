const crypto = require('crypto');
const getSupportedCiphers = async () => {
    return crypto.getCiphers();
}

const getSupportedHashes = async () => {
    return crypto.getHashes();
}
module.exports.getSupportedCiphers = getSupportedCiphers;
module.exports.getSupportedHashes = getSupportedHashes;