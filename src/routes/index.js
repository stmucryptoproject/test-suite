const express = require('express'),
    fs = require('fs'),
    path = require('path'),
    symmetricCrypto = require('./symmetric/sym-crypto'),
    asymmetricCrytpo = require('./asymmetric/asym-crypto'),
    customCrypto = require('./custom/customCipher'),
    symmetricAlgos = require('./symmetric/symmetric-algos.json'),
    asymmetricAlgos = require('./asymmetric/asymmetric-algos.json'),
    router = express.Router();

// -- [ Symmetric Route
router.route('/symmetric')
.get(async(req, res) => {
    const plainText = 'IAMPLAINTEXTREADYTOBEENCRYPTED';

    const response = await Promise.all(symmetricAlgos.map(async (algo, index) => {
        const algoResponse = await symmetricCrypto.encryptText(plainText, algo);
        return algoResponse;
    }));

    res.send(response);
});

// -- [ Asymmetric Route
router.route('/asymmetric')
.get(async(req, res) => {

    const plainText = 'IAMASECRETMESSAGEREADYTOBEENCRYPTED';

    const ecResponses = asymmetricAlgos.ec.map(async (keySize, index) => {
        const algoResponse = await asymmetricCrytpo.encryptText('ec', keySize, plainText);
        return algoResponse;
    });
    
    const rsaResponses = asymmetricAlgos.rsa.map(async (keySize, index) => {
        const algoResponse = await asymmetricCrytpo.encryptText('rsa', keySize, plainText);
        return algoResponse;
    });

    let responses = [...ecResponses, ...rsaResponses];
    const response = await Promise.all(responses);

    res.send(response);
});

// -- [ Custom Route
router.route('/custom')
.get(async(req, res) => {

    // console.log('REQ: ', req.query);

    let cipherData = await customCrypto.crypto(req.query);

     res.send(cipherData);
});

router.route('/settings')
.get(async(req, res) => {
    let retData = {
        sym: [],
        asym: [],
        hash: []
    };

    retData.sym = JSON.parse(fs.readFileSync(path.join(__dirname, 'symmetric', 'symmetric-algos.json')));
    retData.hash = JSON.parse(fs.readFileSync(path.join(__dirname, 'custom', 'cipherSettings.json')));
    
    let tmpRSA = JSON.parse(fs.readFileSync(path.join(__dirname, 'asymmetric', 'asymmetric-algos.json')));
    tmpRSA.ec.forEach((elem) => {
        retData.asym.push('ec-' + elem);
    });
    tmpRSA.rsa.forEach((elem) => {
        retData.asym.push('rsa-' + elem);
    });

    res.send(retData);
});

module.exports = router;