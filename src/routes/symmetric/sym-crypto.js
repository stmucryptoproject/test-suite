const crypto = require('crypto');
const blockSizeMap = require('./block-size-map.json');
const keySizeMap = require('./key-size-map.json');
const {getElapsedTime} = require('../utils/stopwatch');

let keyMap = {

};

exports.encryptText = async(plainText, algo) => {
    try {
        let key, iv, keySize;
        if(!keyMap[algo] || !keyMap[algo].key) { // a key does not exist yet
    
            let keySizeMatches = algo.match('(128)|(192)|(256)');
            let algoMatches = algo.match('(aes)|(bf)|(blowfish)|(camellia)|(cast)|(des)|(idea)|(rc2)|(seed)|(xts)|(rc4)')
            const algoName = algoMatches[0];
            keySize = keySizeMatches && keySizeMatches.length > 0 ? parseInt(keySizeMatches[0], 10): 0;
            if(keySize === 0) { 
                // TODO check the algo and determine keysize
                keySize = keySizeMap[algoName]
            }

            if(algo.includes('xts')) {
                keySize += (blockSizeMap[algoName] * 8);
            }

            key = crypto.randomBytes(keySize/8);
            const ivSize = algo.toLowerCase().includes("ccm") ? 12 : algo.toLowerCase().includes('ecb') ? 0 : blockSizeMap[algoName]? blockSizeMap[algoName]: 0;
            iv = crypto.randomBytes(ivSize);
            keyMap[algo] = { key, iv };
        } else {
            key = keyMap[algo].key;
            iv = keyMap[algo].iv;
        }
    
        let startTime = process.hrtime();
        let cipher = algo.toLowerCase().match('(ccm)|(ctr)|(gcm)') ? 
            crypto.createCipheriv(algo, key, iv, {authTagLength: 16}) : 
            crypto.createCipher(algo, key);
        let ciphered = cipher.update(plainText, 'utf8', 'hex');
        ciphered += cipher.final('hex');
        let elapsedSeconds = parseFloat(getElapsedTime(startTime, 10));
        keyMap[algo].cipherText = ciphered;

        let cipherData = {
            key: key.toString('hex'),
            iv: iv.toString('hex'),
            cipher: ciphered, 
            tag: algo.toLowerCase().match('(ccm)|(gcm)') ? cipher.getAuthTag().toString('hex') : null,
            elapsedSeconds, 
            algo, 
            keySize, 
            plainText
        };
        cipherData = await decrypt(cipherData);
        return cipherData;
    } catch(e){
        console.log(`ERROR for ${algo}: `, e);
        return null;
    }
}

const decrypt = async(cipherData) => {
    let startTime = process.hrtime();
    let decipher = cipherData.algo.toLowerCase().match('(ccm)|(ctr)|(gcm)') ? 
        crypto.createDecipheriv(cipherData.algo, Buffer.from(cipherData.key, 'hex'), Buffer.from(cipherData.iv, 'hex'), {authTagLength: 16}) 
        : crypto.createDecipher(cipherData.algo, Buffer.from(cipherData.key, 'hex'));
    if(cipherData.algo.toLowerCase().match('(ccm)|(gcm)')) {
        decipher.setAuthTag(Buffer.from(cipherData.tag, 'hex'));
    }

    let decryptedText = decipher.update(cipherData.cipher, 'hex', 'utf8');
    try{
        decryptedText += decipher.final();
    } catch (err) {
        console.error('Authentication failed', err);
    }
    let elapsedSeconds = getElapsedTime(startTime, 10);
    cipherData.decryptedText = decryptedText;
    cipherData.decryptElapsedSeconds = parseFloat(elapsedSeconds);
    return cipherData;
};