const crypto = require('crypto');
// use another library for ec encryption since openssl doesn't support it.
// openssl only supports ECDH and ECDSA
const ecies = require('standard-ecies');
const {getElapsedTime} = require('../utils/stopwatch');

const ecCurveMap = {
    160: 'secp160k1',
    224: 'secp224r1', // NIST Curve
    256: 'secp256k1',
    384: 'secp384r1', // NIST Curve
    521: 'secp521r1'  // NIST Curve
}

const ecOptions = {
    hashName: 'sha256',
    hashLength: 32,
    macName: 'sha256',
    macLength: 32,
    symmetricCypherName: 'aes-256-ecb',
    keyFormat: 'uncompressed',
    curveName: ''
}

exports.encryptText = async (algo, keySize, plainText) => {
    
    // keygen operation
    const keyPair = generateKeyPair(algo, keySize);

    const buffer = Buffer.from(plainText, 'utf8');
    let cipher, elapsedSeconds;


    // encrypt with public key
    const startTime = process.hrtime();
    if(algo === 'ec') {
        cipher = ecies.encrypt(keyPair.ecdh.getPublicKey(), buffer, ecOptions);
        // if(cipher) {
            elapsedSeconds = parseFloat(getElapsedTime(startTime, 10));
        // }
    } else {
        cipher = crypto.publicEncrypt({key: keyPair.publicKey}, buffer);
        elapsedSeconds = parseFloat(getElapsedTime(startTime, 10));
    }

    // Only being sent back together so that we can track from the db
    // the length of time for each permutation of algo/keysizes.

    let cipherData = {
        keyPair: keyPair,
        cipher: cipher.toString('hex'), 
        encryptElapsedSeconds: elapsedSeconds, 
        algo, 
        keySize, 
        plainText
    };
    cipherData = await decryptText(cipherData);
    return cipherData;
};

const decryptText = async (cipherData) => {
    let elapsedSeconds, decryptedText;
    if(cipherData.algo === 'ec') {
        const startTime = process.hrtime();
        decryptedText = ecies.decrypt(cipherData.keyPair.ecdh, Buffer.from(cipherData.cipher, 'hex'), ecOptions).toString('utf8');
        elapsedSeconds = parseFloat(getElapsedTime(startTime, 10));
    } else {
        const startTime = process.hrtime();
        decryptedText = crypto.privateDecrypt(cipherData.keyPair.privateKey, Buffer.from(cipherData.cipher, 'hex')).toString('utf8');
        elapsedSeconds = parseFloat(getElapsedTime(startTime, 10));
    }

    cipherData.decryptElapsedSeconds = elapsedSeconds;
    cipherData.decryptedText = decryptedText;
    return cipherData;
};

const generateKeyPair = (algo, keySize) => {
    try {

        const options = {
            modulusLength: parseInt(keySize, 10),
            divisorLength: (algo === 'dsa') ? parseInt(keySize, 10) : null,
            publicKeyEncoding: {
                type: 'spki',
                format: 'pem'
            },
            privateKeyEncoding: {
                type: 'pkcs8',
                format: 'pem'
            }
        }
        let keyPair = {
            publicKey: '',
            privateKey: ''
        }
    
        if(algo === 'ec') {
            ecOptions.curveName = ecCurveMap[keySize]; // Here keysize refers to the prime bit field of the curve
            let startTime = process.hrtime();
            const ecdh = crypto.createECDH(ecOptions.curveName);
            ecdh.generateKeys();
            const elapsedSeconds = parseFloat(getElapsedTime(startTime, 10));
            keyPair = {ecdh, elapsedSeconds};
        } else {
            let startTime = process.hrtime();
            const keysResponse = crypto.generateKeyPairSync(algo, options) // use synchronous version so that we can time it
            const elapsedSeconds = parseFloat(getElapsedTime(startTime, 10));
            keyPair = {publicKey: keysResponse.publicKey, privateKey: keysResponse.privateKey, elapsedSeconds};
        }
    
        return keyPair;
    } catch (err) {
        console.error(`error generating key for ${algo} with keysize of ${keySize}`);
        console.error(err);
    }
}