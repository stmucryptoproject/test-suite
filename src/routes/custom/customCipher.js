let CryptoJS = require('crypto-js'),
    symCipher = require('../symmetric/sym-crypto'),
    asymCipher = require('../asymmetric/asym-crypto'),
    {getElapsedTime} = require('../utils/stopwatch');

function generateKey() {
    var key = "";
    var hex = "0123456789abcdef";

    for (i = 0; i < 64; i++) {
        key += hex.charAt(Math.floor(Math.random() * 16));
    }
    return key;
}

exports.crypto = async(encProperties) => {
    let cipherText = null;

    switch(encProperties.type) {
        case 'symmetric':
            cipherText = await symCipher.encryptText(encProperties.data, encProperties.algo);
            break;
        case 'asymmetric':
            cipherText = await asymCipher.encryptText(encProperties.algo, encProperties.keySize, encProperties.data);
            break;
        default:
            let key = generateKey(),
                retData = {
                    key: null,
                    cipher: null,
                    algo: encProperties.algo,
                    plainText: encProperties.data,
                    elapsedSeconds: null
                },
                startTime = null;

            try {
                startTime = process.hrtime();
                retData.cipher = CryptoJS[encProperties.algo](encProperties.data).toString();
                retData.elapsedSeconds = parseFloat(getElapsedTime(startTime, 10));
            } catch {
                retData.key = key;
                startTime = process.hrtime();
                retData.cipher = CryptoJS[encProperties.algo](encProperties.data, key).toString();
                retData.elapsedSeconds = parseFloat(getElapsedTime(startTime, 10));
            }

            cipherText = retData;
    }

    // console.log('CipherText: ', cipherText);

    return cipherText;
}
